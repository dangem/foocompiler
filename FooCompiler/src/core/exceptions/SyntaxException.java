package core.exceptions;

@SuppressWarnings("serial")
public class SyntaxException extends Exception {

    public SyntaxException(String cause) {
        super(cause);
    }

}
