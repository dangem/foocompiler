package core.exceptions;

@SuppressWarnings("serial")
public class SemanticException extends Exception {

    public SemanticException(String cause) {
        super(cause);
    }

}
