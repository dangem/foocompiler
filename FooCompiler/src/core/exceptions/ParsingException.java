package core.exceptions;

@SuppressWarnings("serial")
public class ParsingException extends Exception {

    public ParsingException() {
        super("Errore durante il parsing");
    }

    public ParsingException(String cause) {
        super(cause);
    }

}
