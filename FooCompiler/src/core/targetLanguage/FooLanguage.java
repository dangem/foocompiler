package core.targetLanguage;

public class FooLanguage implements TargetLanguage {

    private MemoryInstructions memory;
    private MathInstructions math;
    private ConditionalInstructions conditional;
    private UtilityInstructions utility;

    public FooLanguage() {
        this.memory = new Memory();
        this.math = new Math();
        this.conditional = new Conditional();
        this.utility = new Utility();
    }

    @Override
    public String getName() {
        return "Foo Language";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public MathInstructions getMathInstructions() {
        return this.math;
    }

    @Override
    public ConditionalInstructions getConditionalsInstructions() {
        return this.conditional;
    }

    @Override
    public MemoryInstructions getMemoryInstrctions() {
        return this.memory;
    }

    @Override
    public UtilityInstructions getUtilityInstructions() {
        return this.utility;
    }

}
