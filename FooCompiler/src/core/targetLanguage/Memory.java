package core.targetLanguage;

class Memory implements MemoryInstructions {

    @Override
    public String getPushOnStack(String operand) {
        return "push " + operand;
    }

    @Override
    public String getPushOffOnStack(String offset) {
        return "pushoff " + offset;
    }

    @Override
    public String getPopFromStack() {
        return "pop";
    }

    @Override
    public String getLoadLocal(String offset) {
        return "fpload " + offset;
    }

    @Override
    public String getStoreLocal(String offset) {
        return "fpstore " + offset;
    }

}
