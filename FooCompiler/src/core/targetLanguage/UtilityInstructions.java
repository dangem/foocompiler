package core.targetLanguage;

public interface UtilityInstructions {

    public String getFunctionCall(String parameters, String address);
    public String getReturn();
    public String getReturnVal();
    public String getStopMachine();
    public String getPrintFunction();
    public String getEntryFunction();

}
