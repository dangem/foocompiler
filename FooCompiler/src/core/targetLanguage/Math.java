package core.targetLanguage;

class Math implements MathInstructions {

    @Override
    public String getAddOperator() {
        return "add";
    }

    @Override
    public String getMinusOperator() {
        return "sub";
    }

    @Override
    public String getMultiplyOperator() {
        return "mult";
    }

    @Override
    public String getDivideOperator() {
        return "div";
    }

}
