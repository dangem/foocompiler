package core.targetLanguage;

public interface MemoryInstructions {

    public String getPushOnStack(String operand);
    public String getPushOffOnStack(String offset);
    public String getPopFromStack();
    public String getLoadLocal(String offset);
    public String getStoreLocal(String offset);

}
