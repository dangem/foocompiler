package core.targetLanguage;

public interface TargetLanguage {

    public String getName();
    public String getVersion();

    public MathInstructions getMathInstructions();
    public ConditionalInstructions getConditionalsInstructions();
    public MemoryInstructions getMemoryInstrctions();
    public UtilityInstructions getUtilityInstructions();

}
