package core.targetLanguage;

class Conditional implements ConditionalInstructions {

    @Override
    public String getEqualsZero(String address) {
        return "jmpz " + address;
    }

    @Override
    public String getNotEqualsZero(String address) {
        return "jmpnz " + address;
    }

    @Override
    public String getGreaterZero(String address) {
        return "jmpgz " + address;
    }

    @Override
    public String getGreaterEqualZero(String address) {
        return "jmpgez " + address;
    }

    @Override
    public String getLessZero(String address) {
        return "jmplz " + address;
    }

    @Override
    public String getLessEqualZero(String address) {
        return "jmplez " + address;
    }

    @Override
    public String getGoto(String address) {
        return "jmp " + address;
    }

}
