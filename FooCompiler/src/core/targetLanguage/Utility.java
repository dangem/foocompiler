package core.targetLanguage;

class Utility implements UtilityInstructions {

    @Override
    public String getFunctionCall(String parameters, String address) {
        return "call " + parameters + " " + address;
    }

    @Override
    public String getReturn() {
        return "ret";
    }

    @Override
    public String getReturnVal() {
        return "retval";
    }

    @Override
    public String getStopMachine() {
        return "halt";
    }

    @Override
    public String getPrintFunction() {
        return "print";
    }

    @Override
    public String getEntryFunction() {
        return "main";
    }

}
