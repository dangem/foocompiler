package core.targetLanguage;

public interface ConditionalInstructions {

    public String getEqualsZero(String address);
    public String getNotEqualsZero(String address);
    public String getGreaterZero(String address);
    public String getGreaterEqualZero(String address);
    public String getLessZero(String address);
    public String getLessEqualZero(String address);
    public String getGoto(String address);

}
