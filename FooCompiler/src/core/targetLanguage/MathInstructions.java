package core.targetLanguage;

public interface MathInstructions {

    public String getAddOperator();
    public String getMinusOperator();
    public String getMultiplyOperator();
    public String getDivideOperator();

}
