package core.symbolTables;

public class VariableInfo {

    private String name;
    private String fpOffset;

    public VariableInfo() {
    }

    public VariableInfo(String name) {
        this.name = name;
    }

    public VariableInfo(String name, String fpOffset) {
        this.name = name;
        this.fpOffset = fpOffset;
    }

    public String getFpOffset() {
        return fpOffset;
    }

    public void setFpOffset(String fpOffset) {
        this.fpOffset = fpOffset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        return this.name + " " + this.fpOffset;
    }

}
