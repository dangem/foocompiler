package core.symbolTables;

import java.util.HashMap;

public class LocalFunctionTable {

    private HashMap<String, VariableInfo> variables;
    private int offsetCount;

    public LocalFunctionTable() {
        this.variables = new HashMap<String, VariableInfo>();
        this.offsetCount = 0;
    }

    public void add(VariableInfo variable) {
        variable.setFpOffset(String.valueOf(offsetCount));
        offsetCount += 1;
        variables.put(variable.getName(), variable);
    }

    public boolean contains(String varName) {
        return variables.containsKey(varName);
    }

    public VariableInfo getVariable(String varName) {
        return variables.get(varName);
    }

}
