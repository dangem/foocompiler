package core.symbolTables;

import java.util.HashMap;

public class FunctionsTable {

    private HashMap<String, FunctionInfo> functions;

    public FunctionsTable() {
        this.functions = new HashMap<String, FunctionInfo>();
    }

    public void addFunction(FunctionInfo function) {
        this.functions.put(function.getName(), function);
    }

    public FunctionInfo getFunction(String name) {
        return this.functions.get(name);
    }

    public boolean contains(String name) {
        return this.functions.containsKey(name);
    }

}
