package core.symbolTables;

public class FunctionInfo {

    private String name;
    private boolean voidReturnValue;
    private boolean isDefined;
    private boolean isDeclared;
    private int numParameters;

    public FunctionInfo() {
    }

    public FunctionInfo(String name, boolean voidReturnValue) {
        this.name = name;
        this.voidReturnValue = voidReturnValue;
        this.numParameters = 0;
        this.isDeclared = false;
        this.isDefined = false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setNumParameters(int numParameters) {
        this.numParameters = numParameters;
    }

    public int getNumParameters() {
        return this.numParameters;
    }

    public void setVoidReturn(boolean value) {
        this.voidReturnValue = value;
    }

    public boolean isVoidReturn() {
        return this.voidReturnValue;
    }

    public boolean isDefined() {
        return this.isDefined;
    }

    public boolean isDeclared() {
        return this.isDeclared;
    }

    public void setDefined() {
        this.isDefined = true;
    }

    public void setDeclared() {
        this.isDeclared = true;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String toString() {
        String returnValue;
        if (this.voidReturnValue)
            returnValue = "void";
        else
            returnValue = "int";

        String signature = new String();
        signature = signature.concat(returnValue + " " + name + " ( ");
        for (int i = 0; i < this.numParameters; i++)
            signature = signature.concat("int ");

        if (this.numParameters == 0)
            signature = signature.concat("void )");
        else
            signature = signature.concat(")");

        return signature;
    }

}
