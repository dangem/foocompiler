package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.lexer.token.Token;
import core.targetLanguage.MathInstructions;
import core.targetLanguage.MemoryInstructions;

public class ExpressionParser extends AbstractSubsectionParser {

    /* ************************************** */
    private MathInstructions mathInstr;
    private MemoryInstructions memInstr;
    private Token operand;
    /* ************************************** */

    public ExpressionParser() {
        /* ************************************** */
        this.mathInstr = language.getMathInstructions();
        this.memInstr = language.getMemoryInstrctions();
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        EXPRESSION();
    }

    public void EXPRESSION() throws ParsingException, IOException, SemanticException {
        FACTOR();
        EXPRESSION_1();
    }

    public void EXPRESSION_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("plusOperator")) {
            match("plusOperator");
            FACTOR();
            EXPRESSION_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getAddOperator());
            /* ************************************** */
        }
        else if (currentToken.getId().equals("minusOperator")) {
            match("minusOperator");
            FACTOR();
            EXPRESSION_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getMinusOperator());
            /* ************************************** */
        }
        else {
            EPSILON();
        }
    }

    public void FACTOR() throws ParsingException, IOException, SemanticException {
        TERM();
        FACTOR_1();
    }

    public void FACTOR_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("multiplyOperator")) {
            match("multiplyOperator");
            TERM();
            FACTOR_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getMultiplyOperator());
            /* ************************************** */
        }
        else if (currentToken.getId().equals("divideOperator")) {
            match("divideOperator");
            TERM();
            FACTOR_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getDivideOperator());
            /* ************************************** */
        }
        else {
            EPSILON();
        }
    }

    public void TERM() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openRoundBracket")) {
            match("openRoundBracket");
            EXPRESSION();
            match("closeRoundBracket");
        }
        else if (currentToken.getId().equals("intToken")) {
            match("intToken");

            /* ************************************** */
            operand = previousToken;
            codeWriter.writeln(memInstr.getPushOnStack(operand.getLexeme()));
            /* ************************************** */
        }
        else if (currentToken.getId().equals("idToken")) {
            match("idToken");

            /* ************************************** */
            callFuncInExp = true;
            unknownName = previousToken.getLexeme();
            /* ************************************** */

            functionInvocationParser.parse();

            /* ************************************** */
            callFuncInExp = false;
            /* ************************************** */
        }
        else 
            throw new ParsingException("Prevista un'espressione ma l'argomento è di"
                    + " tipo " + currentToken.getId());
    }

}
