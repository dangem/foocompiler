package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.targetLanguage.ConditionalInstructions;
import core.utility.UnresolvedAddress;

public class IfElseParser extends AbstractSubsectionParser {

    /* ************************************** */
    private String unresAddrLab;
    private ConditionalInstructions condInstr;
    private UnresolvedAddress currentUnresAddress;
    private UnresolvedAddress previousUnresAddress;
    private UnresolvedAddress unresolvedAddrTmp;
    /* ************************************** */

    public IfElseParser() {
        /* ************************************** */
        this.unresAddrLab = UnresolvedAddress.unresolvedAddressLabel;
        this.condInstr = language.getConditionalsInstructions();
        this.currentUnresAddress = new UnresolvedAddress();
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        IF_ELSE_DEFINITION();
    }

    /* QUI CI SONO DUE INDIRIZZI IRRISOLTI: IL PRIMO NEL CASO IN CUI LA CONDIZIONE 
     * DELL'IF NON SIA SODDISFATTA (QUINDI SI DEVE SALTARE SUBITO DOPO IL BLOCCO
     * DELL'IF CORRENTE, IN UN EVENTUALE ELSE OPPURE NO); IL SECONDO RIGUARDA
     * QUALE ISTRUZIONE ESEGUIRE SUBITO DOPO IL BLOCCO DI QUESTO IF. SE C'È UN ELSE LO SI
     * SALTA, ALTRIMENTI SI CONTINUA CON IL NORMALE FLUSSO (IN QUESTO CASO LA SITUAZIONE
     * VIENE GESTITA COMUNQUE CON UN SALTO PER QUESTIONE D'UNIFORMITÀ CON IL CASO
     * DELL'ELSE). 
     */
    public void IF_ELSE_DEFINITION() throws ParsingException, IOException, SemanticException {
        match("ifOperator");
        match("openRoundBracket");
        boolExpressionParser.parse();
        match("closeRoundBracket");

        /* ************************************** */
        currentUnresAddress = new UnresolvedAddress(unresAddrLab 
                + String.valueOf(unresolvedAddressCount));
        codeWriter.writeln(condInstr.getEqualsZero(currentUnresAddress.getLabel()));
        unresolvedAddressStack.push(currentUnresAddress);
        unresolvedAddressCount += 1;
        /* ************************************** */

        match("openCurlyBracket");
        statementParser.parse();
        match("closeCurlyBracket");

        /* ************************************** */
        currentUnresAddress = new UnresolvedAddress(unresAddrLab 
                + String.valueOf(unresolvedAddressCount));
        codeWriter.writeln(condInstr.getGoto(currentUnresAddress.getLabel()));
        unresolvedAddressStack.push(currentUnresAddress);
        unresolvedAddressCount += 1;
        /* ************************************** */

        IF_ELSE_DEFINITION_1();
    }

    public void IF_ELSE_DEFINITION_1() throws ParsingException, IOException, SemanticException {

        if (currentToken.getId().equals("elseOperator")) {
            match("elseOperator");

            /* IL POP PERCHÈ BISOGNA PRIMA RISOLVERE L'INDIRIZZO NEL CASO IN CUI
             * NON SI SODDISFATTA LA CONDIZIONE DELL'IF (QUINDI BISOGNA SALTARE IN 
             * QUESTO ELSE; SUCCESSIVAMENTE SI RISOLVE IL SECONDO INDIRIZZO, CIOÈ QUELLO
             * RELATIVO ALLA FINE DEL BLOCCO IF, IN CUI SI DEVE RIPRENDERE SUBITO DOPO
             * QUESTO ELSE
             */
            /* ************************************** */
            previousUnresAddress = unresolvedAddressStack.pop();
            unresolvedAddrTmp = unresolvedAddressStack.pop();
            UnresolvedAddress tmp = new UnresolvedAddress(unresolvedAddrTmp.getLabel()
                    , String.valueOf(codeWriter.getAddressCount() + 1));
            addressResolver.add(tmp);
            addressResolver.resolve(tmp.getLabel());
            unresolvedAddressStack.push(previousUnresAddress);
            /* ************************************** */

            match("openCurlyBracket");
            statementParser.parse();
            match("closeCurlyBracket");

            /* ************************************** */
            unresolvedAddrTmp = unresolvedAddressStack.pop();
            //			System.out.println(unresolvedAddrTmp.getLabel() + " " 
            //					+ String.valueOf(codeWriter.getAddressCount() + 1));
            tmp = new UnresolvedAddress(unresolvedAddrTmp.getLabel()
                    , String.valueOf(codeWriter.getAddressCount() + 1));
            addressResolver.add(tmp);
            addressResolver.resolve(tmp.getLabel());
            /* ************************************** */
        }
        else { 
            EPSILON();

            /* IN QUESTO CASO L'INDIRIZZO DELLA FINE DEL BLOCCO IF E DALLA CONDIZIONE
             * DELL'IF NON SODDISFATTA, COINCIDONO. QUESTO PERCHÈ NON C'È NESSUN BLOCCO
             * ELSE.
             */
            /* ************************************** */
            unresolvedAddrTmp = unresolvedAddressStack.pop();
            UnresolvedAddress tmp = new UnresolvedAddress(unresolvedAddrTmp.getLabel()
                    , String.valueOf(codeWriter.getAddressCount() + 1));
            addressResolver.add(tmp);;
            addressResolver.resolve(tmp.getLabel());
            unresolvedAddrTmp = unresolvedAddressStack.pop();
            tmp = new UnresolvedAddress(unresolvedAddrTmp.getLabel()
                    , String.valueOf(codeWriter.getAddressCount() + 1));
            addressResolver.add(tmp);
            addressResolver.resolve(tmp.getLabel());
            /* ************************************** */
        }
    }

}
