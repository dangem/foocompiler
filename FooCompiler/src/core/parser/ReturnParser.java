package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.targetLanguage.UtilityInstructions;

public class ReturnParser extends AbstractSubsectionParser {

    /* ************************************** */
    private UtilityInstructions utilInstr;
    /* ************************************** */

    public ReturnParser() {
        /* ************************************** */
        this.utilInstr = language.getUtilityInstructions();
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        RETURN_DEFINITION();
    }

    public void RETURN_DEFINITION() throws ParsingException, IOException, SemanticException {
        match("returnOperator");
        RETURN_DEFINITION_1();

    }

    public void RETURN_DEFINITION_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openRoundBracket")
                || currentToken.getId().equals("intToken")
                || currentToken.getId().equals("idToken")) {

            /* ************************************** */
            if (currentFunction.isVoidReturn())
                throw new SemanticException("funzione " + currentFunction.getName()
                        + " ritorna void");
            else if (currentFunction.getName().equals(utilInstr.getEntryFunction()))
                throw new SemanticException("funzione " + currentFunction.getName()
                        + " ritorna solamente void");

            /* ************************************** */

            expressionParser.parse();
            match("semicolon");

            /* ************************************** */
            codeWriter.writeln(utilInstr.getReturnVal());
            /* ************************************** */
        }
        /* UNA FUNZIONE NON VOID PUÒ ANCHE RITORNARE QUALCOSA DEL TIPO "return;"
         * IN QUESTO CASO RITORNA QUELLO CHE È ATTUALMENTE IN CIMA ALLO STACK,
         * CIOÈ QUALCOSA DI INDEFINITO.
         */
        else if (currentToken.getId().equals("semicolon")) {
            match("semicolon");

            /* ************************************** */
            if (currentFunction.getName().equals(utilInstr.getEntryFunction())) {
                codeWriter.writeln(utilInstr.getStopMachine());
            }
            else {
                if (currentFunction.isVoidReturn())
                    codeWriter.writeln(utilInstr.getReturn());
                else
                    codeWriter.writeln(utilInstr.getReturnVal());
            }
            /* ************************************** */
        }
        else 
            throw new ParsingException("Prevista un'espressione ma l'argomento è di" +
                    " tipo " + currentToken.getId());
    }

}
