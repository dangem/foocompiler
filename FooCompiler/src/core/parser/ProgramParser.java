package core.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.symbolTables.FunctionInfo;
import core.symbolTables.LocalFunctionTable;
import core.symbolTables.VariableInfo;
import core.targetLanguage.UtilityInstructions;
import core.utility.UnresolvedAddress;

public class ProgramParser extends AbstractSubsectionParser {

    /* ************************************** */
    private int numParameterCurFunction;
    private UtilityInstructions utilInstr;
    private FunctionInfo funcTmp;
    private List<VariableInfo> parameters;
    /* ************************************** */

    public ProgramParser() {
        /* ************************************** */
        this.parameters = new ArrayList<VariableInfo>();
        this.numParameterCurFunction = 0;
        this.utilInstr = language.getUtilityInstructions();
        this.funcTmp = null;
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        PROGRAM();
    }

    public void PROGRAM() throws ParsingException, IOException, SemanticException {
        FUNC_DEFINITION();
        PROGRAM_1();
    }

    public void PROGRAM_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("intType")
                || currentToken.getId().equals("voidType")) {
            FUNC_DEFINITION();
            PROGRAM_1();
        }
        else if (currentToken.getId().equals("EOF"))
            match("EOF");
        else
            throw new ParsingException("Prevista una dichiarazione o definizione" 
                    + " di funzione ma l'argomento è di tipo " + currentToken.getId());
    }

    public void FUNC_DEFINITION() throws ParsingException, IOException, SemanticException {

        /* ************************************** */
        currentFunction = new FunctionInfo();
        /* ************************************** */

        TYPES();
        match("idToken");

        /* ************************************** */
        currentFunction.setName(previousToken.getLexeme());
        /* ************************************** */

        match("openRoundBracket");
        ARGS_DEF();

        /* ************************************** */
        numParameterCurFunction = 0; // RESET PER LA PROSSIMA FUNZIONE
        /* ************************************** */

        match("closeRoundBracket");
        FUNC_DEFINITION_1();
    }

    public void TYPES() throws ParsingException, IOException {
        if (currentToken.getId().equals("intType")) {
            match("intType");

            /* ************************************** */
            currentFunction.setVoidReturn(false);
            /* ************************************** */
        }
        else if (currentToken.getId().equals("voidType")) {
            match("voidType");

            /* ************************************** */
            currentFunction.setVoidReturn(true);
            /* ************************************** */
        }
        else
            throw new ParsingException("Tipo di ritorno " + currentToken.getId()
                    + " non ammesso");

    }

    public void FUNC_DEFINITION_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openCurlyBracket")) {

            /* ************************************** */
            localTable = new LocalFunctionTable(); // NUOVO SCOPE
            localFuncVariblesCount = 0; // ANCORA NESSUNA VARIABILE LOCALE DEFINITA
            
            /* AGGIUNGE I PARAMETRI NELLA TABELLA DEI SIMBOLI LOCALI
             * NON C'È BISOGNO DI METTERLI NELLO STACK PERCHÈ DI QUESTO SI È GIÀ
             * OCCUPATO LA FUNZIONE CHIAMANTE
             */
            for (VariableInfo curParameter : parameters) {
                if (localTable.contains(curParameter.getName()))
                    throw new SemanticException("variabile " + curParameter.getName()
                            + " già dichiarata");
                localTable.add(curParameter);
            }
            // SVUOLA LA LISTA PER UNA PROSSIMA DEFINIZIONE DI FUNZIONE
            this.parameters.clear();

            funcTmp = functionsTable.getFunction(currentFunction.getName());
            if (funcTmp != null) {
                // NON SONO PERMESSE DEFINIZIONI MULTIPLE, OVVIAMENTE
                if (funcTmp.isDefined())
                    throw new SemanticException("funzione " + funcTmp.getName()
                            + " già dichiarata");
                // CONTROLLO SUL VALORE DI RITORNO
                if (funcTmp.isVoidReturn() != currentFunction.isVoidReturn())
                    throw new SemanticException("funzione " + funcTmp.getName()
                            + " dichiarata con un parametro di ritorno e definita" +
                            " con un altro");
                // CONTROLLO SUI PARAMETRI
                if (funcTmp.getNumParameters() != currentFunction.getNumParameters())
                    throw new SemanticException("funzione " + funcTmp.getName()
                            + " dichiarata con " + funcTmp.getNumParameters() + 
                            " parametri e definita con " 
                            + currentFunction.getNumParameters());
            }
            /* ************************************** */

            match("openCurlyBracket");

            /* ************************************** */
            if (currentFunction.getName().equals(utilInstr.getEntryFunction())) {
                isEntryFuncDefined = true;
                if (currentFunction.getNumParameters() != 0 
                        || currentFunction.isVoidReturn() == false)
                    throw new SemanticException("funzione " + currentFunction.getName()
                            + " deve essere: void " + currentFunction.getName()
                            + "(void)");
            }

            // SI STA DEFINENDO LA FUNZIONE
            currentFunction.setDeclared();
            currentFunction.setDefined();
            functionsTable.addFunction(currentFunction);
            // +1 PERCHÈ LA FUNZIONE PARTE DAL PROSSIMO INDIRIZZO
            UnresolvedAddress tmp = new UnresolvedAddress(
                    UnresolvedAddress.unresolvedAddressLabel + currentFunction.getName() 
                    , String.valueOf(codeWriter.getAddressCount() + 1));
            addressResolver.add(tmp);
            addressResolver.resolve(tmp.getLabel());
            /* ************************************** */

            varSectionParser.parse();
            match("closeCurlyBracket");

            /* ************************************** */
            // SE LA FUNZIONE È IL "MAIN" ALLORA SI AGGIUNGE L'ISTRUZIONE DI HALT ALLA FINE
            if (currentFunction.getName().equals(this.utilInstr.getEntryFunction())) 
                codeWriter.writeln(utilInstr.getStopMachine());
            /* FIXME: IN QUESTI DUE CASI, SE IL PROGRAMMATORE HA GIÀ SCRITTO DELLE "return"
             * A FINE DELLA FUNZIONE, ALLORA CI SARANNO DELLE ISTRUZIONI RIDONDATI.
             * PERÒ SE NON LE HA SCRITTE ALLORA ASSICURANO CHE IL PROGRAMMA TERMINI
             * QUALCHE MODO.
             */
            else if (currentFunction.isVoidReturn())
                codeWriter.writeln(utilInstr.getReturn());
            /* RITORNA QUALCOSA DI INDEFINITO, QUELLO CHE È IN CIMA ALLO STACK
             * IN QUEL MOMENTO.
             */
            else if (currentFunction.isVoidReturn() == false)
                codeWriter.writeln(utilInstr.getReturnVal());
            currentFunction = null;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("semicolon")) {
            match("semicolon");

            /* ************************************** */
            if (currentFunction.getName().equals(utilInstr.getEntryFunction()))
                throw new SemanticException("funzione " + currentFunction.getName()
                        + " può essere solamente definita");
            // I PARAMETRI NON SERVONO, È SOLO UNA DICHIARAZIONE DI FUNZIONE
            this.parameters.clear();
            // LA FUNZIONE È STATO SOLO DICHIARATA
            // NON SONO PERMESSE DICHIARAZIONI MULTIPLE
            this.funcTmp = functionsTable.getFunction(currentFunction.getName());
            if (funcTmp != null) {
                if (funcTmp.isDeclared())
                    throw new SemanticException("funzione " + funcTmp.getName()
                            + " già dichiarata");
            }

            currentFunction.setDeclared();
            functionsTable.addFunction(currentFunction);
            currentFunction = null;
            /* ************************************** */
        }
        else 
            throw new ParsingException("Prevista una dichirazione o definizione"
                    + " di funzione ma l'argomento è di tipo " + currentToken.getId());
    }

    public void ARGS_DEF() throws ParsingException, IOException {
        if (currentToken.getId().equals("voidType")) {
            match("voidType");

            /* ************************************** */
            currentFunction.setNumParameters(0);
            /* ************************************** */
        }
        else if (currentToken.getId().equals("intType")) {
            FIRST_ARG_DEF();

            /* ************************************** */
            currentFunction.setNumParameters(this.numParameterCurFunction);
            /* ************************************** */
        }
        else 
            throw new ParsingException("Tipo argomento " + currentToken.getId() 
                    + " non ammesso");
    }

    public void FIRST_ARG_DEF() throws ParsingException, IOException {
        match("intType");
        match("idToken");

        /* ************************************** */
        parameters.add(new VariableInfo(previousToken.getLexeme(),
                String.valueOf(numParameterCurFunction)));
        this.numParameterCurFunction += 1;
        /* ************************************** */

        FIRST_ARG_DEF_1();
    }

    public void FIRST_ARG_DEF_1() throws ParsingException, IOException {
        if (currentToken.getId().equals("comma")) {
            OTHER_ARG_DEF();
        }
        else 
            EPSILON();
    }

    public void OTHER_ARG_DEF() throws ParsingException, IOException {
        match("comma");
        match("intType");
        match("idToken");

        /* ************************************** */
        parameters.add(new VariableInfo(previousToken.getLexeme(),
                String.valueOf(numParameterCurFunction)));
        this.numParameterCurFunction += 1;
        /* ************************************** */

        OTHER_ARG_DEF_1();
    }

    public void OTHER_ARG_DEF_1() throws ParsingException, IOException {
        if (currentToken.getId().equals("comma")) {
            OTHER_ARG_DEF();
        }
        else 
            EPSILON();
    }

}
