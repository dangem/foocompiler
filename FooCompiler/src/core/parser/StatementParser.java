package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;

public class StatementParser extends AbstractSubsectionParser {

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        STATEMENT();
    }

    public void STATEMENT() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("ifOperator")) {
            ifElseParser.parse();
            STATEMENT();
        }
        else if (currentToken.getId().equals("whileOperator")) {
            whileParser.parse();
            STATEMENT();
        }
        else if (currentToken.getId().equals("returnOperator")) {
            returnParser.parse();
            STATEMENT();
        }
        else if (currentToken.getId().equals("idToken")) {
            compundExpParser.parse();
            STATEMENT();
        }
        else if (currentToken.getId().equals("semicolon")) {
            doNothingParser.parse();
            STATEMENT();
        }
        else 
            EPSILON();
    }

}
