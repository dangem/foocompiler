package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.targetLanguage.ConditionalInstructions;
import core.targetLanguage.MathInstructions;
import core.targetLanguage.MemoryInstructions;

public class BoolExpressionParser extends AbstractSubsectionParser {

    /* ************************************** */
    private enum CodeCondOperator {
        IDENTICAL_OP, DIFFERERT_OP, GREATER_OP, GREATER_EQ_OP,
        MINOR_OP, MINOR_EQ_OP
    }

    private CodeCondOperator currentOperator;
    private MathInstructions mathInstr;
    private MemoryInstructions memInstr;
    private ConditionalInstructions condInstr;
    /* ************************************** */


    public BoolExpressionParser() {
        /* ************************************** */
        this.mathInstr = language.getMathInstructions();
        this.memInstr = language.getMemoryInstrctions();
        this.condInstr = language.getConditionalsInstructions();
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        BOOL_EXP();
    }

    public void BOOL_EXP() throws ParsingException, IOException, SemanticException {
        BOOL_FACTOR();
        BOOL_EXP_1();
    }

    public void BOOL_EXP_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("orOperator")) {
            match("orOperator");
            BOOL_FACTOR();
            BOOL_EXP_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getAddOperator());
            this.writeCodeZeroOrOne();
            /* ************************************** */
        }
        else
            EPSILON();
    }

    public void BOOL_FACTOR() throws ParsingException, IOException, SemanticException {
        BOOL_TERM();
        BOOL_FACTOR_1();
    }

    public void BOOL_FACTOR_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("andOperator")) {
            match("andOperator");
            BOOL_TERM();
            BOOL_FACTOR_1();

            /* ************************************** */
            codeWriter.writeln(mathInstr.getMultiplyOperator());
            this.writeCodeZeroOrOne();
            /* ************************************** */
        }
        else
            EPSILON();
    }

    public void BOOL_TERM() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openRoundBracket")
                || currentToken.getId().equals("intToken")
                || currentToken.getId().equals("idToken")) {
            CONDITIONAL_EXP();
        }
        else if (currentToken.getId().equals("openSquareBracket")) {
            match("openSquareBracket");
            BOOL_EXP();
            match("closeSquareBracket");
        }
        else 
            throw new ParsingException("Prevista un'espressione ma l'argomento"
                    + " è di tipo " + currentToken.getId());
    }

    public void CONDITIONAL_EXP() throws ParsingException, IOException, SemanticException {
        expressionParser.parse();
        CONDITIONAL_EXP_1();
    }

    public void CONDITIONAL_EXP_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("identicalOperator")
                || currentToken.getId().equals("differentOperator")
                || currentToken.getId().equals("greaterOperator")
                || currentToken.getId().equals("greaterEqualOperator")
                || currentToken.getId().equals("minorOperator")
                || currentToken.getId().equals("minorEqualOperator")) {
            CONDITIONAL_OPERATOR();
            expressionParser.parse();

            /* ************************************** */
            this.writeCodeCurrentOperator();
            /* ************************************** */
        }
        else {
            EPSILON();

            /* ************************************** */
            this.writeCodeZeroOrOne();
            /* ************************************** */
        }
    }

    public void CONDITIONAL_OPERATOR() throws ParsingException, IOException {
        if (currentToken.getId().equals("identicalOperator")) {
            match("identicalOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.IDENTICAL_OP;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("differentOperator")) {
            match("differentOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.DIFFERERT_OP;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("greaterOperator")) {
            match("greaterOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.GREATER_OP;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("greaterEqualOperator")) {
            match("greaterEqualOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.GREATER_EQ_OP;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("minorOperator")) {
            match("minorOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.MINOR_OP;
            /* ************************************** */
        }
        else if (currentToken.getId().equals("minorEqualOperator")) {
            match("minorEqualOperator");

            /* ************************************** */
            currentOperator = CodeCondOperator.MINOR_EQ_OP;
            /* ************************************** */
        }
        else 
            throw new ParsingException("Previsto un operatore booleano ma"
                    + " l'argomento è di tipo " + currentToken.getId());
    }

    /* ************************************** */
    private void writeCodeZeroOrOne() throws IOException {
        /* +4 PERCHE' L'ISTRUZIONE CORRENTE NON E' ANCORA STATA SCRITTA
         * (E' PROPRIO QUESTO JMPZ)
         */
        codeWriter.writeln(condInstr
                .getEqualsZero(String.valueOf(codeWriter
                        .getAddressCount() + 4)));
        codeWriter.writeln(memInstr.getPopFromStack());
        codeWriter.writeln(memInstr.getPushOnStack("1"));
    }

    // +4 E +5 PERCHE' L'ISTRUZIONE CORRENTE NON E' ANCORA STATA SCRITTA
    private void writeCodeCurrentOperator() throws IOException {
        switch (currentOperator) {
        case IDENTICAL_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            codeWriter.writeln(condInstr
                    .getEqualsZero(String.valueOf(codeWriter
                            .getAddressCount() + 5)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("0"));
            codeWriter.writeln(condInstr.getGoto(String.valueOf(codeWriter
                    .getAddressCount() + 4)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("1"));
            break;

        case DIFFERERT_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            this.writeCodeZeroOrOne();
            break;

        case GREATER_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            codeWriter.writeln(condInstr
                    .getGreaterZero(String.valueOf(codeWriter
                            .getAddressCount() + 5)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("0"));
            codeWriter.writeln(condInstr.getGoto(String.valueOf(codeWriter
                    .getAddressCount() + 4)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("1"));
            break;

        case GREATER_EQ_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            codeWriter.writeln(condInstr
                    .getGreaterEqualZero(String.valueOf(codeWriter
                            .getAddressCount() + 5)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("0"));
            codeWriter.writeln(condInstr.getGoto(String.valueOf(codeWriter
                    .getAddressCount() + 4)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("1"));
            break;

        case MINOR_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            codeWriter.writeln(condInstr
                    .getLessZero(String.valueOf(codeWriter
                            .getAddressCount() + 5)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("0"));
            codeWriter.writeln(condInstr.getGoto(String.valueOf(codeWriter
                    .getAddressCount() + 4)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("1"));
            break;

        case MINOR_EQ_OP:
            codeWriter.writeln(mathInstr.getMinusOperator());
            codeWriter.writeln(condInstr
                    .getLessEqualZero(String.valueOf(codeWriter
                            .getAddressCount() + 5)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("0"));
            codeWriter.writeln(condInstr.getGoto(String.valueOf(codeWriter
                    .getAddressCount() + 4)));
            codeWriter.writeln(memInstr.getPopFromStack());
            codeWriter.writeln(memInstr.getPushOnStack("1"));
            break;
        }
    }
    /* ************************************** */

}








