package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.targetLanguage.ConditionalInstructions;
import core.utility.UnresolvedAddress;

public class WhileParser extends AbstractSubsectionParser {

    /* ************************************** */
    private String unresAddrLab;
    private UnresolvedAddress initWhileAddr;
    private UnresolvedAddress currentUnresAddress;
    private ConditionalInstructions condInstr;
    /* ************************************** */

    public WhileParser() {
        /* ************************************** */
        this.unresAddrLab = UnresolvedAddress.unresolvedAddressLabel;
        this.currentUnresAddress = null;
        this.initWhileAddr = null;
        this.condInstr = language.getConditionalsInstructions();
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        WHILE_DEFINITION();
    }

    public void WHILE_DEFINITION() throws ParsingException, IOException, SemanticException {
        match("whileOperator");

        /* ************************************** */
        // +1 PERCHÈ PROSSIMA ISTRUZIONE, INIZIO CONTROLLO CONDIZIONE DEL WHILE.
        initWhileAddr = new UnresolvedAddress(unresAddrLab 
                + String.valueOf(unresolvedAddressCount)
                , String.valueOf(codeWriter.getAddressCount() + 1));
        unresolvedAddressStack.push(initWhileAddr);
        addressResolver.add(initWhileAddr);
        unresolvedAddressCount += 1;
        /* ************************************** */

        match("openRoundBracket");
        boolExpressionParser.parse();
        match("closeRoundBracket");

        /* ************************************** */
        currentUnresAddress = new UnresolvedAddress(unresAddrLab 
                + String.valueOf(unresolvedAddressCount));
        /* SE LA CONDIZIONE NON È SODDISFATTA SALTA A QUESTO INDIRIZZO ANCORA IGNOTO
         * (SAREBBE QUELLO SUBITO DOPO QUESTO WHILE).
         */
        codeWriter.writeln(condInstr.getEqualsZero(currentUnresAddress.getLabel()));
        unresolvedAddressStack.push(currentUnresAddress);
        unresolvedAddressCount += 1;
        /* ************************************** */

        match("openCurlyBracket");
        statementParser.parse();

        /* ************************************** */
        UnresolvedAddress unresTmp = unresolvedAddressStack.pop();
        // L'INDIRIZZO initWhile È IL SECONDO DALLA CIMA DELLO STACK, A QUESTO PUNTO.
        codeWriter.writeln(condInstr.getGoto(unresolvedAddressStack.pop().getAddress()));
        // RISOLVE L'INDIRIZZO SUBITO DOPO IL WHILE (CHE È GIÀ IN unresTmp)
        // +1 PERCHÈ PROSSIMA ISTRUZIONE
        unresTmp.setAddress(String.valueOf(codeWriter.getAddressCount() + 1));
        addressResolver.add(unresTmp);
        addressResolver.resolve(unresTmp.getLabel());
        /* ************************************** */

        match("closeCurlyBracket");
    }


}
