package core.parser;

import java.io.IOException;
import java.util.Stack;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.lexer.Lexer;
import core.symbolTables.FunctionsTable;
import core.targetLanguage.FooLanguage;
import core.utility.UnresolvedAddress;
import core.utility.UtilityInterface;

public class FooLangParser extends AbstractSubsectionParser implements Parser {

    private String fileToParse;

    public FooLangParser() throws IOException {
        /* ************************************** */
        isEntryFuncDefined = false;
        funcInvocationLocalMember = new Stack<Object>();
        language = new FooLanguage();
        utilities = new UtilityInterface();
        unresolvedAddressStack = utilities.getUnresolvedAddressStack();
        addressResolver = utilities.getAddressResolver();
        codeWriter = utilities.getCodeWriter(outputFileCode);
        functionsTable = new FunctionsTable();
        currentFunction = null;
        unresolvedAddressCount = 0;
        callFuncInExp = false;
        localFuncVariblesCount = 0;
        /* ************************************** */

        expressionParser = new ExpressionParser();
        compundExpParser = new CompoundExpParser();
        functionInvocationParser = new FunctionInvocationParser();
        boolExpressionParser = new BoolExpressionParser();
        ifElseParser = new IfElseParser();
        programParser = new ProgramParser();
        statementParser = new StatementParser();
        whileParser = new WhileParser();
        varDefinitionParser = new VarDefinitionParser();
        varSectionParser = new VarSectionParser();
        doNothingParser = new DoNothingParser();
        returnParser = new ReturnParser();
    }

    public void parse() throws ParsingException, IOException, SemanticException {
        lexer = new Lexer(fileToParse);
        currentToken = lexer.getNextToken();

        /* ************************************** */
        // GESTISCE IL SALTO NELLA FUNZIONE D'INIZIO (IL "main" ESSENZIALMENTE)
        UnresolvedAddress entryFuncUnresolv = new UnresolvedAddress(
                UnresolvedAddress.unresolvedAddressLabel 
                + language.getUtilityInstructions().getEntryFunction());
        codeWriter.writeln(language.getConditionalsInstructions()
                .getGoto(entryFuncUnresolv.getLabel()));
        /* ************************************** */

        programParser.parse();
        match("EOF");

        /* ************************************** */
        if (isEntryFuncDefined == false)
            throw new SemanticException("funzione " + language.getUtilityInstructions()
                    .getEntryFunction() + " non definita");
        
        codeWriter.writeOnFile(outputFileCode);
        /* ************************************** */
    }

    public void setFileToParse(String fileToParse) throws IOException {
        this.fileToParse = fileToParse;
    }
    
    /* ************************************** */
    public void setOutputFile(String fileOutput) {
        outputFileCode = fileOutput;
    }
    /* ************************************** */

}
