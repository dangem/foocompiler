package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.symbolTables.VariableInfo;
import core.targetLanguage.MemoryInstructions;

public class CompoundExpParser extends AbstractSubsectionParser {

    /* ************************************** */
    private MemoryInstructions memInstr;
    private VariableInfo curVariable;
    /* ************************************** */

    public CompoundExpParser() {
        /* ************************************** */
        this.memInstr = language.getMemoryInstrctions();
        this.curVariable = null;
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        COMPOUND_EXP();
    }

    public void COMPOUND_EXP() throws ParsingException, IOException, SemanticException {
        match("idToken");

        /* ************************************** */
        // SOLO DOPO SI POTRÀ SAPERE SE È UN NOME DI VARIABILE O UN NOME DI FUNZIONE
        unknownName = previousToken.getLexeme();
        /* ************************************** */

        COMPOUND_EXP_1();
        match("semicolon");

        /* ************************************** */
        unknownName = null;
        /* ************************************** */
    }

    public void COMPOUND_EXP_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("equalOperator")) {
            match("equalOperator");

            /* ************************************** */
            // unknownName È IL NOME DI UNA VARIABILE 
            if (localTable.contains(unknownName) == false) {
                throw new SemanticException("variabile " + unknownName
                        + " non dichiarata");
            }
            curVariable = localTable.getVariable(unknownName);
            callFuncInExp = true;
            /* ************************************** */

            expressionParser.parse();

            /* ************************************** */
            callFuncInExp = false;
            // SALVA IL NUOVO VALORE DELLA VARIABILE
            codeWriter.writeln(memInstr.getStoreLocal(curVariable.getFpOffset()));
            /* ************************************** */
        }
        /* QUI SI FORZA AD INVOCARE UNA FUNZIONE SEBBENE FunciontInvocationParser
         * PER VENIRE INCONTRO AD ExpressionParser, PERMETTE DI SCRIVERE NOMI DI ID
         * SENZA LE PARENTESI TONDE A SEGUIRE
         */
        else if (currentToken.getId().equals("openRoundBracket")) {
            functionInvocationParser.parse();
        }
        else throw new ParsingException("Prevista un'invocazione di funzione oppure"
                + " un assegnamento ma l'argomento è di tipo " + currentToken.getId());

    }

}
