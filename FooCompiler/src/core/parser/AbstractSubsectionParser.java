package core.parser;

import java.io.IOException;
import java.util.Stack;
import core.exceptions.ParsingException;
import core.lexer.Lexer;
import core.lexer.token.Token;
import core.symbolTables.FunctionInfo;
import core.symbolTables.FunctionsTable;
import core.symbolTables.LocalFunctionTable;
import core.targetLanguage.TargetLanguage;
import core.utility.AddressResolver;
import core.utility.CodeWriter;
import core.utility.UnresolvedAddressStack;
import core.utility.UtilityInterface;

public abstract class AbstractSubsectionParser implements Parserable {

    /* ************************************** */
    // PER LA GENERAZIONE DEL CODICE MACCHINA 
    protected static String outputFileCode;
    protected static Token previousToken;
    protected static CodeWriter codeWriter;
    protected static TargetLanguage language;
    protected static UtilityInterface utilities;
    
    // IL PUNTO DI INIZIO DEL PROGRAMMA
    protected static boolean isEntryFuncDefined;

    /* SE LA FUNZIONE VIENE CHIAMATA IN UN'ESPRESSIONE ALLORA DEVE AVERE UN VALORE
     * DI RITORNO, ALTRIMENTI NON NECESSARIAMENTE.
     */
    protected static boolean callFuncInExp;
    /* O VARIABILE A SINISTRA DELL'OPERATORE UGUALE IN UNA CompoundExpression
     * O NOME DI FUNZIONE DA INVOCARE 
     */
    protected static String unknownName;
    /* LA SEGUENTE VARIABILE TIENE LE SPECIFICHE DELLA FUNZIONE CORRENTE
     * PER TUTTO IL TEMPO DEL SUO PARSING (CIOÈ DA QUANDO SI APRE LA PARENTESI
     * GRAFFA PER DEFINIRLA, A QUANDO LA SI CHIUDE).
     */
    protected static FunctionInfo currentFunction;
    /* CON LE CHIAMATE INNESTATE SI POSSONO PERDERE I VALORI CHE SI STAVANO
     * USANDO NELL'INVOCAZIONE CORRENTE, QUINDI SERVE UNO STACK PER RICORDARE. 
     * ESEMPIO:
     * NELL'INVOCARE UNA FUNZIONE, LE SI PASSA COME PARAMETRO IL VALORE DI RITORNO
     * DI UN'ALTRA FUNZIONE. IN QUESTO CASO SI PERDEREBBERO LE INFORMAZIONI RIGUARDO
     * LA PRIMA FUNZIONE CHIAMATA.
     */
    protected static Stack<Object> funcInvocationLocalMember;
    /* LE VARIABILI LOCALI DEFINITE IN UNA FUNZIONE. SERVE PER CALCOLARE 
     * L'OFFSET DA FP.
     */
    protected static int localFuncVariblesCount;
    protected static LocalFunctionTable localTable;
    protected static FunctionsTable functionsTable;

    protected static long unresolvedAddressCount;
    protected static UnresolvedAddressStack unresolvedAddressStack;
    protected static AddressResolver addressResolver;
    /* ************************************** */

    protected static Token currentToken;
    protected static Lexer lexer;

    protected static Parserable expressionParser;
    protected static Parserable compundExpParser;
    protected static Parserable functionInvocationParser;
    protected static Parserable boolExpressionParser;
    protected static Parserable ifElseParser;
    protected static Parserable programParser;
    protected static Parserable statementParser;
    protected static Parserable whileParser;
    protected static Parserable varDefinitionParser;
    protected static Parserable varSectionParser;
    protected static Parserable doNothingParser;
    protected static Parserable returnParser;

    public AbstractSubsectionParser() {
    }

    public void match(String tokenId) throws ParsingException, IOException {
        if (currentToken.getId().equals(tokenId)) {
            previousToken = currentToken;
            currentToken = lexer.getNextToken();
        }
        else
            throw new ParsingException("Previsto un " + tokenId + " ma l'argomento" 
                    + " è di tipo " + currentToken.getId());
    }

    protected void EPSILON() {
    }

}





