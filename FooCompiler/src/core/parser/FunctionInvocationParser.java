package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.symbolTables.VariableInfo;
import core.targetLanguage.MemoryInstructions;
import core.targetLanguage.UtilityInstructions;
import core.utility.UnresolvedAddress;

public class FunctionInvocationParser extends AbstractSubsectionParser {

    /* ************************************** */
    private MemoryInstructions memInstr;
    private UtilityInstructions utilInstr;
    private int numParameterInvokedFunc;
    private int realNumParameterInvokedFunc;
    private String parameterOffset;
    private String localUnknownName;
    /* ************************************** */

    public FunctionInvocationParser() {
        /* ************************************** */
        this.memInstr = language.getMemoryInstrctions();
        this.utilInstr = language.getUtilityInstructions();
        this.numParameterInvokedFunc = 0;
        this.realNumParameterInvokedFunc = 0;
        this.parameterOffset = null;
        this.localUnknownName = null;
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        /* ************************************** */
        funcInvocationLocalMember.push(new Integer(numParameterInvokedFunc));
        funcInvocationLocalMember.push(new Integer(realNumParameterInvokedFunc));
        funcInvocationLocalMember.push(parameterOffset);
        funcInvocationLocalMember.push(localUnknownName);
        this.localUnknownName = unknownName;
        this.numParameterInvokedFunc = 0;
        this.realNumParameterInvokedFunc = 0;
        this.parameterOffset = null;
        /* ************************************** */

        GENERAL_FUNCTION_INVOCATION();

        /* ************************************** */
        localUnknownName = (String) funcInvocationLocalMember.pop();
        parameterOffset = (String) funcInvocationLocalMember.pop();
        realNumParameterInvokedFunc = ((Integer) funcInvocationLocalMember.pop()).intValue();
        numParameterInvokedFunc = ((Integer) funcInvocationLocalMember.pop()).intValue();
        /* ************************************** */
    }

    public void GENERAL_FUNCTION_INVOCATION() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openRoundBracket")) {

            /* ************************************** */
            /* GESTISCE L'ISTRUZIONE PRINT. PROPRIO PER QUESTO MOTIVO QUESTA FUNZIONE
             * NON SOFFRE DEL PROBLEMA DEL PASSAGGIO DEI PARAMETRI DATI DAL VALORE
             * DI RITORNO DI ALTRI FUNZIONI. ESEMPIO:
             * f1(f2(x)); NORMALMENTE HA UN BUG, MA print(f2(x)) FUNZIONA!
             */
            if (localUnknownName.equals(utilInstr.getPrintFunction())) {
                if (callFuncInExp) 
                    throw new SemanticException("funzione " + localUnknownName 
                            + " ritorna void");
                match("openRoundBracket");
                expressionParser.parse();
                match("closeRoundBracket");
                codeWriter.writeln(utilInstr.getPrintFunction());
                return;
            }

            // A QUESTO PUNTO SI SA CHE unknownName È IL NOME DI UNA FUNZIONE
            if (functionsTable.contains(localUnknownName) == false) {
                throw new SemanticException("funzione " + unknownName 
                        + " non dichiarata");
            }
            else if (functionsTable.getFunction(localUnknownName)
                    .isVoidReturn() && callFuncInExp) {
                throw new SemanticException("funzione " + localUnknownName 
                        + " ritorna void");
            }
            /* ************************************** */

            FUNCTION_INVOCATION();
        }
        else {

            /* ************************************** */
            /* SI ARRIVA IN QUESTO PUNTO SOLO DA ExpressionParser PER CAPIRE
             * SE UN IdToken È UNA VARIABILE OPPURE UNA FUNZIONE. IN QUESTO
             * CASO È IL NOME DI UNA VARIABILE
             */
            if (localTable.contains(localUnknownName) == false)
                throw new SemanticException("variabile " + unknownName 
                        + " non dichiarata");
            VariableInfo var = localTable.getVariable(localUnknownName);
            codeWriter.writeln(memInstr.getLoadLocal(var.getFpOffset()));
            /* ************************************** */

            EPSILON();
        }
    }

    public void FUNCTION_INVOCATION() throws ParsingException, IOException, SemanticException {
        match("openRoundBracket");
        FUNCTION_INVOCATION_1();
    }

    public void FUNCTION_INVOCATION_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("openRoundBracket")
                || currentToken.getId().equals("intToken")
                || currentToken.getId().equals("idToken")) {
            FIRST_ARG_INVOKE();
            match("closeRoundBracket");
        }
        else if (currentToken.getId().equals("closeRoundBracket")) {
            match("closeRoundBracket");
        }
        else 
            throw new ParsingException("Previsto un parametro di funzione ma" +
            		" l'argomento è di tipo " + currentToken.getId());

        /* ************************************** */
        String funcName = localUnknownName;
        realNumParameterInvokedFunc = functionsTable
                .getFunction(funcName).getNumParameters();
        if (numParameterInvokedFunc != realNumParameterInvokedFunc) {
            throw new SemanticException("funzione " + funcName
                    + " chiamata con " + String.valueOf(numParameterInvokedFunc)
                    + " invece che con " + String.valueOf(realNumParameterInvokedFunc)
                    + " parametri");
        }
        
        /* SE L'INDIRIZZO DELLA FUNZIONE È DISPONIBILE ORA ALLORA LO SI USA, 
         * ALTRIMENTI VERRÀ RISOLTO DOPO QUANDI SI DEFINIRÀ LA FUNZIONE (LÌ C'È
         * UN addressResolver.resolve()).
         */
        
        UnresolvedAddress tmp = addressResolver
                .get(UnresolvedAddress.unresolvedAddressLabel
                + funcName);
        if (tmp == null)
            codeWriter.writeln(utilInstr.getFunctionCall
                    (String.valueOf(realNumParameterInvokedFunc)
                            , UnresolvedAddress.unresolvedAddressLabel + funcName));
        else
            codeWriter.writeln(utilInstr.getFunctionCall
                    (String.valueOf(realNumParameterInvokedFunc)
                            , tmp.getAddress()));
        /* ************************************** */

    }

    public void FIRST_ARG_INVOKE() throws ParsingException, IOException, SemanticException {
        expressionParser.parse();

        /* ************************************** */
        /* -1 PERCHÈ POI VERRÀ FATTO UN POP E LA CALL VERRÀ CHIAMATA 
         * CON LO STACK IN QUELLO STATO
         */
        parameterOffset = String.valueOf(2 + numParameterInvokedFunc - 1);
        codeWriter.writeln(memInstr.getPushOffOnStack(parameterOffset));
        /* NEL CALCOLARE IL VALORE DEL NUOVO PARAMETRO SI DEVE ELIMINARE QUELLO 
         * PRECEDENTE DALLO STACK ALTRIMENTI LO STACK POINTER POTREBBE ANDARE
         * A SOVRAPPORSI CON I PARAMETRI APPENA MESSI NELLO STACK
         */
        codeWriter.writeln(memInstr.getPopFromStack());
        this.numParameterInvokedFunc += 1;
        /* ************************************** */

        FIRST_ARG_INVOKE_1();
    }

    public void FIRST_ARG_INVOKE_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("comma")) {
            OTHER_ARG_INVOKE();
        }
        else
            EPSILON();
    }

    public void OTHER_ARG_INVOKE() throws ParsingException, IOException, SemanticException {
        match("comma");
        expressionParser.parse();

        /* ************************************** */
        /* -1 PERCHÈ POI VERRÀ FATTO UN POP E LA CALL VERRÀ CHIAMATA 
         * CON LO STACK IN QUELLO STATO
         */
        parameterOffset = String.valueOf(2 + numParameterInvokedFunc - 1);
        codeWriter.writeln(memInstr.getPushOffOnStack(parameterOffset));
        /* NEL CALCOLARE IL VALORE DEL NUOVO PARAMETRO SI DEVE ELIMINARE QUELLO 
         * PRECEDENTE DALLO STACK ALTRIMENTI LO STACK POINTER POTREBBE ANDARE
         * A SOVRAPPORSI CON I PARAMETRI APPENA MESSI NELLO STACK
         */
        codeWriter.writeln(memInstr.getPopFromStack());
        this.numParameterInvokedFunc += 1;
        /* ************************************** */

        OTHER_ARG_INVOKE_1();
    }

    public void OTHER_ARG_INVOKE_1() throws ParsingException, IOException, SemanticException {
        if (currentToken.getId().equals("comma")) {
            OTHER_ARG_INVOKE();
        }
        else
            EPSILON();
    }

}
