package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;

public interface Parserable {

    public void parse() throws ParsingException, SemanticException, IOException;

}
