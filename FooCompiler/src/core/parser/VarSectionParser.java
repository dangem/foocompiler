package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;

public class VarSectionParser extends AbstractSubsectionParser {

    @Override
    public void parse() throws ParsingException, SemanticException, IOException {
        VAR_SECTION();
    }
    
    public void VAR_SECTION() throws ParsingException, SemanticException, IOException {
        if (currentToken.getId().equals("intType")) {
            varDefinitionParser.parse();
            VAR_SECTION();
        }
        else if (currentToken.getId().equals("ifOperator")
                || currentToken.getId().equals("whileOperator")
                || currentToken.getId().equals("returnOperator")
                || currentToken.getId().equals("idToken")
                || currentToken.getId().equals("closeCurlyBracket")) {
            statementParser.parse();
        }
        else 
            throw new ParsingException("Previsto un intType ma l'argomento è di"
            		+ " tipo " + currentToken.getId());
    }

}
