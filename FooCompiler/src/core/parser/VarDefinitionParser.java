package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.symbolTables.VariableInfo;

public class VarDefinitionParser extends AbstractSubsectionParser {

    /* ************************************** */
    private VariableInfo variable;
    int variableOffset;
    /* ************************************** */

    public VarDefinitionParser() {
        /* ************************************** */
        this.variable = null;
        this.variableOffset = 0;
        /* ************************************** */
    }

    @Override
    public void parse() throws ParsingException, IOException, SemanticException {
        VAR_DEFINITION();
    }

    public void VAR_DEFINITION() throws ParsingException, IOException, SemanticException {
        match("intType");
        match("idToken");

        /* ************************************** */
        variable = new VariableInfo(previousToken.getLexeme());
        if (localTable.contains(variable.getName()))
            throw new SemanticException("variabile " + variable.getName()
                    + " già dichiarata");
        variableOffset = Integer.valueOf(localFuncVariblesCount) 
                + currentFunction.getNumParameters();
        variable.setFpOffset(String.valueOf(variableOffset));
        localTable.add(variable);
        localFuncVariblesCount += 1;
        /* ************************************** */

        match("equalOperator");
        /* SE LE VARIABILI LOCALI VENGONO TUTTE E SOLE DEFINITE ALL'INIZIO DELLA
         * FUNZIONE ALLORA NON C'È BISOGNO DI FARE UN fpStore PERCHÈ IL VALORE
         * MESSO SULLO STACK DALL'ESPRESSIONE CHE DEFINISCE QUELLA VERIABILE
         * SARÀ PROPRIO NELLA POSIZIONE IN CUI SI TROVA LA VARIABILE
         * (QUESTO APPUNTO PERCHÈ LE VARIABILI VENGONO TUTTE DEFINITE SEQUENZIALMENTE
         * ALL'INIZIO).
         */
        expressionParser.parse();
        match("semicolon");
    }

}
