package core.parser;

import java.io.IOException;

public interface Parser extends Parserable {

    public void setFileToParse(String fileToParse) throws IOException;

}
