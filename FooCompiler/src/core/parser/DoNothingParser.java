package core.parser;

import java.io.IOException;
import core.exceptions.ParsingException;

public class DoNothingParser extends AbstractSubsectionParser {

    @Override
    public void parse() throws ParsingException, IOException {
        DO_NOTHING();
    }

    public void DO_NOTHING() throws ParsingException, IOException {
        match("semicolon");
    }

}
