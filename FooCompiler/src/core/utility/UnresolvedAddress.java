package core.utility;

public class UnresolvedAddress {

    public static String unresolvedAddressLabel = "AddressUnresolved_";

    private String label;
    private String address;

    public UnresolvedAddress() {
    }

    public UnresolvedAddress(String label) {
        this.label = label;
        this.address = "-";
    }

    public UnresolvedAddress(String label, String address) {
        this.label = label;
        this.address = address;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return this.label.toString() + " " + this.address.toString();
    }

    @Override
    public int hashCode() {
        return this.label.hashCode();
    }

}
