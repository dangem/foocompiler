package core.utility;

import java.io.CharArrayWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CustomCharArrayWriter extends CharArrayWriter implements CodeWriter {
    
    private long instructionCount;
    private boolean firstInstr;
    
    public CustomCharArrayWriter() {
        this.instructionCount = -1;
        this.firstInstr = true;
    }

    @Override
    public void writeln(String str) throws IOException {
        if (firstInstr) {
            super.write(str);
            firstInstr = false;
        }
        else
            super.write("\n" + str);
        this.instructionCount += 1;
    }

    @Override
    public long getAddressCount() {
        return instructionCount;
    }

    @Override
    public void setAddressCount(long instructionCount) {
        this.instructionCount = instructionCount;
    }


    @Override
    public void writeOnFile(String fileName) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName);
        fileWriter.write(this.buf);
        fileWriter.close();
    }
    
    /* RIMPIAZZA IN buf (BUFFER DI QUESTO WRITER) OCCORRENZE DELLA STRINGA toSearch
     * CON OCCORRENZE DI toReplace. I CARATTERI DI DIFFERENZA TRA LE DUE STRINGHE
     * (toSearch DEVE AVERE UNA LUNGHEZZA >= DI toReplace) VENGONO RIMPIAZZATI CON
     * ' '. TUTTO CIÒ PER MOTIVI DI EFFICIENZA. SE LA CONDIZIONE PRECEDENTE NON È
     * SODDISFATTA ALLORA SI DEVE USARE UN METODO ALTERNATIVO.
     */
    public int fastSearchAndReplace(String toSearch, String toReplace) {
        if (toReplace.length() > toSearch.length() || toSearch.length() <= 0) {
            System.err.println("problema lunghezza stringhe in " +
            		"CustomCharArrayWriter.searchAndReplace");
            System.exit(1);
        }
        
        int replaced = 0;
        int k = 0, j = 0;
        int startSearchingIndex = 0;
        int toSearchLen = toSearch.length();
        int toReplaceLen = toReplace.length();
        boolean found = false;
        
        for(int i = 0; i < buf.length; i++) {
            if (buf[i] == toSearch.charAt(0)) {
                startSearchingIndex = i;
                k = i + 1;
                j = 1;
                while (j < toSearchLen && k < buf.length 
                        && buf[k] == toSearch.charAt(j) ) {
                    k++;
                    j++;
                }
                
                if (j >= toSearchLen)
                    found = true;
                
                if (found) {
                    j = 0;
                    k = startSearchingIndex;
                    while (j < toSearchLen) {
                        if (j < toReplaceLen)
                            buf[k] = toReplace.charAt(j);
                        else
                            buf[k] = ' ';
                        j++;
                        k++;
                    }
                    replaced++;
                    found = false;
                }
                i += toSearchLen;
            }
        }
        
        return replaced;
    }
    
}
