package core.utility;

import java.io.IOException;

public interface CodeWriter {

    public void writeln(String str) throws IOException;
    public long getAddressCount();
    public void setAddressCount(long instructionCount);
    public void writeOnFile(String fileName) throws IOException;

}
