package core.utility;

public interface UnresolvedAddressStack {

    public void push(UnresolvedAddress unresolvedAddress);
    public UnresolvedAddress pop();

}
