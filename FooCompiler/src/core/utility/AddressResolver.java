package core.utility;

public interface AddressResolver {

    /* PER USARE QUESTA FUNZIONE SI DEVE ESSERE SICURI CHE L'OGGETTO CERCATO SIA
     * NEL DATABASE
     */
    public void resolve(String key);
    
    public UnresolvedAddress get(String key);
    public void add(UnresolvedAddress unresolvedAddress);
    public void resolveAll(String fileToResolve);

}
