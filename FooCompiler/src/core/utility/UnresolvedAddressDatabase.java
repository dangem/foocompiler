package core.utility;

import java.util.HashMap;
import java.util.Stack;

class UnresolvedAddressDatabase implements UnresolvedAddressStack
    ,AddressResolver {

    private CustomCharArrayWriter writer;
    private Stack<UnresolvedAddress> addressStack;
    private HashMap<String, UnresolvedAddress> addressMap;

    public UnresolvedAddressDatabase(CustomCharArrayWriter writer) {
        this.writer = writer;
        this.addressMap = new HashMap<String, UnresolvedAddress>();
        this.addressStack = new Stack<UnresolvedAddress>();
    }

    public void push(UnresolvedAddress unresolvedAddress) {
        addressStack.push(unresolvedAddress);
    }

    public UnresolvedAddress pop() {
        return addressStack.pop();
    }

    public void resolve(String key) {
        UnresolvedAddress tmp = addressMap.get(key);
        writer.fastSearchAndReplace(tmp.getLabel(), tmp.getAddress());
    }
    
    public void add(UnresolvedAddress unresolvedAddress) {
        this.addressMap.put(unresolvedAddress.getLabel(), unresolvedAddress);
    }

    @Override
    public UnresolvedAddress get(String key) {
        return addressMap.get(key);
    }
    
    @Override
    public void resolveAll(String fileToResolve) {
        for (String label : addressMap.keySet()) 
            writer.fastSearchAndReplace(label, addressMap.get(label).getAddress());
    }

}
