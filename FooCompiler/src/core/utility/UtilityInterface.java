package core.utility;

import java.io.IOException;

public class UtilityInterface {

    private UnresolvedAddressDatabase database;
    private CustomCharArrayWriter writer;

    public UtilityInterface() {
        this.writer = new CustomCharArrayWriter();
        this.database = new UnresolvedAddressDatabase(this.writer);
    }

    public UnresolvedAddressStack getUnresolvedAddressStack() {
        return this.database;
    }

    public AddressResolver getAddressResolver() {
        return this.database;
    }

    public CodeWriter getCodeWriter(String filePath) throws IOException {
        return this.writer;
    }

}
