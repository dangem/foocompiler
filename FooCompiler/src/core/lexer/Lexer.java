package core.lexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import core.lexer.token.BoolOperatorToken;
import core.lexer.token.ConditionalOperatorToken;
import core.lexer.token.EOFToken;
import core.lexer.token.IdToken;
import core.lexer.token.IntegerToken;
import core.lexer.token.MathOperatorToken;
import core.lexer.token.ParenthasisToken;
import core.lexer.token.PunctuationToken;
import core.lexer.token.Token;
import core.lexer.token.UnknownToken;

public class Lexer {

    private File file;
    private BufferedReader inputStream;

    private Character currentChar;
    // SERVE PER L'EOF CHE NON E' RAPPRESENTABILE CON UN char
    private int currentCharIntValue;

    private long lineNumber;

    public Lexer(String fileToParse) throws IOException {
        file = new File(fileToParse);
        inputStream = new BufferedReader(new FileReader(file));

        lineNumber = 0;
        // LEGGE IL PRIMO CARATTERE PER "AVVIARE" L'ANALIZZATORE LESSICALE
        readNextChar();
    }

    /* LA CONVEZIONE E' DI LASCIARE LA VARIABILE currentChar SUL PROSSIMO
     * CARATTERE DA LEGGERE
     */
    public Token getNextToken() throws IOException {
        while (isSkipChar())
            readNextChar();

        Token token = null;
        String lexeme = new String();

        // TOKEN ID
        if (CustomCharacter.isLetter(currentChar)) {
            while (CustomCharacter.isLetterOrDigit(currentChar)) {
                lexeme = lexeme.concat(new String(currentChar.toString()));
                this.readNextChar();
            }
            token = new IdToken(lexeme);
        }
        // TOKEN NUMERICO
        else if (CustomCharacter.isDigit(currentChar)) {
            while (CustomCharacter.isDigit(currentChar)) {
                lexeme = lexeme.concat(new String(currentChar.toString()));
                this.readNextChar();
            }
            token = new IntegerToken(lexeme);
        }
        // = OPPURE ==
        else if (CustomCharacter.isEqual(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            this.readNextChar();
            if (CustomCharacter.isEqual(currentChar)) {
                lexeme = lexeme.concat(new String(currentChar.toString()));
                token = new ConditionalOperatorToken(lexeme);
                this.readNextChar();
            }
            else {
                token = new MathOperatorToken(lexeme);
            }
        }
        // OPERATORI ARITMETICI DI BASE
        else if (CustomCharacter.isMathOperator(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            token = new MathOperatorToken(lexeme);
            this.readNextChar();
        }
        // OPERATORI CONDIZIONALI
        else if (CustomCharacter.isComparisonOperator(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            this.readNextChar();
            if (CustomCharacter.isEqual(currentChar)) {
                lexeme = lexeme.concat(new String(currentChar.toString()));
                token = new ConditionalOperatorToken(lexeme);
                this.readNextChar();
            }
            // L'OPERATORE UNARIO "!" NON E' CONTEMPLATO
            else if (CustomCharacter.isExclamationPoint(lexeme.charAt(0)) == false) {
                token = new ConditionalOperatorToken(lexeme);
            }
            else 
                token = new UnknownToken(lexeme);
        }
        // VIRGOLA OPPURE PUNTO E VIRGOLA
        else if (CustomCharacter.isComma(currentChar) 
                || CustomCharacter.isSemicolon(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            token = new PunctuationToken(lexeme);
            this.readNextChar();
        }
        // UNA QUALSIASI PARENTESI APERTA OPPURE CHIUSA
        else if (CustomCharacter.isOpenParenthesis(currentChar)
                || CustomCharacter.isCloseParenthesis(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            token = new ParenthasisToken(lexeme);
            this.readNextChar();
        }
        else if (CustomCharacter.isAmpersand(currentChar)
                || CustomCharacter.isOr(currentChar)) {
            lexeme = lexeme.concat(new String(currentChar.toString()));
            this.readNextChar();
            if (CustomCharacter.isAmpersand(currentChar)
                    || CustomCharacter.isOr(currentChar)) {
                lexeme = lexeme.concat(new String(currentChar.toString()));
                token = new BoolOperatorToken(lexeme);
                this.readNextChar();
            }
            else 
                token = new UnknownToken(lexeme);
        }
        // EOF 
        else if (CustomCharacter.isEOF(currentCharIntValue)) {
            token = new EOFToken(lexeme);
        }
        // ALTRIMENTI TOKEN NON CONTEMPLATO
        else 
            token = new UnknownToken(lexeme);

        return token;
    }

    protected void readNextChar() throws IOException {
        try {
            currentCharIntValue = inputStream.read();
        }
        catch (IOException e) {
            throw new IOException("Impossibile leggere dal file di input");
        }
        currentChar = (char) currentCharIntValue;
    }

    private boolean isSkipChar() {
        if (CustomCharacter.isNewLine(currentChar)) {
            this.lineNumber += 1;
            return true;
        }

        return CustomCharacter.isSpace(currentChar)
                || CustomCharacter.isTabulation(currentChar);
    }

    public long getCurrentLineNumber() {
        return this.lineNumber;
    }

    public void closeStream() throws IOException {
        try {
            this.inputStream.close();
        }
        catch (IOException e) {
            throw new IOException("Impossibile chiudere lo stream di input");
        }
    }

}
