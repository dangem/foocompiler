package core.lexer;

public class CustomCharacter {

    public static boolean isDigit(char c) {
        return Character.isDigit(c);
    }

    public static boolean isLetter(char c) {
        return Character.isLetter(c) || c == '_';
    }   

    public static boolean isLetterOrDigit(char c) {
        return Character.isLetterOrDigit(c) || c == '_';
    }

    public static boolean isSpace(char c) {
        return c == ' ';
    }

    public static boolean isTabulation(char c) {
        return c == '\t';
    }

    public static boolean isNewLine(char c) {
        return c == '\n';
    }

    public static boolean isMathOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    public static boolean isEqual(char c) {
        return c == '=';
    }

    public static boolean isComparisonOperator(char c) {
        return c == '!' || c == '<' || c == '>';
    }

    public static boolean isExclamationPoint(char c) {
        return c == '!';
    }

    public static boolean isOpenParenthesis(char c) {
        return c == '(' || c == '[' || c == '{';
    }

    public static boolean isCloseParenthesis(char c) {
        return c == ')' || c == ']' || c == '}';
    }

    public static boolean isComma(char c) {
        return c == ',';
    }

    public static boolean isSemicolon(char c) {
        return c == ';';
    }

    public static boolean isEOF(int c) {
        return c == -1;
    }

    public static int getNumericValue(char c) {
        return Character.getNumericValue(c);
    }

    public static boolean isOr(char c) {
        return c == '|';
    }

    public static boolean isAmpersand(char c) {
        return c == '&';
    }

}
