package core.lexer.token;

public class MathOperatorToken extends StringToken {

    public MathOperatorToken(String lexeme) {
        super("", lexeme);
        this.setLexeme(lexeme);
    }

}
