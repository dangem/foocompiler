package core.lexer.token;

public class EOFToken extends StringToken {

    public EOFToken(String lexeme) {
        super("EOF", lexeme);
        this.setLexeme(lexeme);
    }

    @Override
    public String toString() {
        return "EOF";
    }

}
