package core.lexer.token;

public abstract class Token {

    protected String id;
    protected String lexeme;

    public Token(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getLexeme() {
        return this.lexeme;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    @Override
    public String toString() {
        return this.lexeme.toString();
    }

}
