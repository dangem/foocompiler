package core.lexer.token;

public class PunctuationToken extends StringToken {

    public PunctuationToken(String lexeme) {
        super("", lexeme);
        this.setLexeme(lexeme);
    }

}
