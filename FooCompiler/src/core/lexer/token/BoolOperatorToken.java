package core.lexer.token;

public class BoolOperatorToken extends StringToken {

    public BoolOperatorToken(String lexeme) {
        super("", lexeme);
        this.setLexeme(lexeme);
    }

}
