package core.lexer.token;

public class IdToken extends StringToken {

    public IdToken(String lexeme) {
        super("idToken", lexeme);
        this.setLexeme(lexeme);
    }

}
