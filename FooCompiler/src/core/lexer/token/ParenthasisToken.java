package core.lexer.token;

public class ParenthasisToken extends StringToken {

    public ParenthasisToken(String lexeme) {
        super("", lexeme);
        this.setLexeme(lexeme);
    }

}
