package core.lexer.token;

public class UnknownToken extends StringToken {

    public UnknownToken(String lexeme) {
        super("unknownToken", lexeme);
    }

}
