package core.lexer.token;

import core.lexer.ReservedWords;

public abstract class StringToken extends Token {

    private String value;

    public StringToken(String id, String value) {
        super(id);

        /* CONTROLLO SULLE PAROLE CHIAVI, OPERATORI E ALTRI COSTRUTTI STANDARD DEL
         * LINGUAGGIO
         */
        String checkedId = ReservedWords.getInstance().checkWord(value);
        if (checkedId != null)
            this.id = checkedId;

        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.id.toString() + ": " + super.toString();
    }

}
