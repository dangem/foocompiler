package core.lexer.token;

public class IntegerToken extends NumericToken {

    public IntegerToken(String lexeme) {
        super("intToken", Integer.valueOf(lexeme));
        this.setLexeme(lexeme);
    }

    public String toString() {
        return this.id.toString() + ": " + super.toString();
    }

}
