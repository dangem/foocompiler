package core.lexer.token;

public class NumericToken extends Token {

    private Number value;

    public NumericToken(String id, Number value) {
        super(id);
        this.value = value;
    }

    public void setValue(Number value) {
        this.value = value;
    }

    public Number getValue() {
        return this.value;
    }

}
