package core.lexer.token;

public class ConditionalOperatorToken extends StringToken {

    public ConditionalOperatorToken(String lexeme) {
        super("", lexeme);
        this.setLexeme(lexeme);
    }

}
