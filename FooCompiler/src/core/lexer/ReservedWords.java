package core.lexer;

import java.util.HashMap;

public class ReservedWords {

    private static ReservedWords instance = null;

    private static HashMap<String, String> reserved;

    private ReservedWords() {
        reserved = new HashMap<String, String>();
        reserved.put("if", "ifOperator");
        reserved.put("else", "elseOperator");
        reserved.put("while", "whileOperator");
        reserved.put("return", "returnOperator");
        reserved.put("int", "intType");
        reserved.put("void", "voidType");
        reserved.put("&&", "andOperator");
        reserved.put("||", "orOperator");
        reserved.put("==", "identicalOperator");
        reserved.put("!=", "differentOperator");
        reserved.put(">", "greaterOperator");
        reserved.put(">=", "greaterEqualOperator");
        reserved.put("<", "minorOperator");
        reserved.put("<=", "minorEqualOperator");
        reserved.put("=", "equalOperator");
        reserved.put("+", "plusOperator");
        reserved.put("-", "minusOperator");
        reserved.put("*", "multiplyOperator");
        reserved.put("/", "divideOperator");
        reserved.put("(", "openRoundBracket");
        reserved.put("[", "openSquareBracket");
        reserved.put("{", "openCurlyBracket");
        reserved.put(")", "closeRoundBracket");
        reserved.put("]", "closeSquareBracket");
        reserved.put("}", "closeCurlyBracket");
        reserved.put(",", "comma");
        reserved.put(";", "semicolon");
    }

    public static ReservedWords getInstance() {
        if (instance == null)
            instance = new ReservedWords();
        return instance;
    }

    public String checkWord(String word) {
        return reserved.get(word);
    }

}
