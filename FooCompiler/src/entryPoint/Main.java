package entryPoint;

import java.io.IOException;
import core.exceptions.ParsingException;
import core.exceptions.SemanticException;
import core.parser.FooLangParser;

public class Main {

    /* IL PRIMO ARGOMENTO DA LINA DI COMANDO È IL FILE DI CUI FARE IL PARSING, IL 
     * SECONDO È IL FILE IN CUI SCRIVERE IL CODICE MACCHINA
     */
    public static void main(String[] args) throws IOException {
        FooLangParser parser = new FooLangParser();
        
        try {
            parser.setFileToParse(args[0]);
            parser.setOutputFile(args[1]);
            parser.parse();
            System.out.println("Parsing eseguito con successo");
        } catch (ParsingException e) {
            System.err.println(e);
        } catch (IOException e) {
            System.err.println(e);
        } catch (SemanticException e) {
            System.err.println(e);
        }
    }

}
